# coding: utf-8   <- dzięki temu polskie litery będą działać bez problemu
# rozszerzenie pliku: .py
# <- oznacza komentarz
# komentarze są ignorowane
# (tak, jakby ich nie było)
# polecenia wykonywane są kolejno linijka po linijce od góry do dołu

print "Witajcie!"
print "Życzę przyjemnych wrażeń :)"

# celem uruchomienia:
# 1. Zapisz program na dysku
# 2. z wiersza poleceń przejdź do odpowiedniego katalogu (polecenie cd)
# 3. wpisz: python nazwa.py (lub: python2 nazwa.py)

# INTERPRETER
# wywołanie samego 'python' lub 'python2' powoduje uruchomienie interpretera
# interpreter pozwala na testowanie fragmentów kodu
# polecam instalację interpretera ipython - o wiele przyjemniejszy w użyciu

print 2+2

# notacja w rodzaju:
wynik = 2**10
# oznacza, że NAJPIERW obliczamy wartość wyrażenia po prawej stronie znaku = (znak ten nazywamy operatorem przypisania), a później do wyliczonej wartości "przyklejamy" etykietkę o nazwie "wynik". Od tej pory możemy się posługiwać tą etykietą zamiast konkretną wartością
# zapis:
wynik = wynik + 5
# powoduje (zgodnie z powyższym) obliczenie wartości wyrażenia, a NASTĘPNIE "przyklejenie" do tej wartości etykiety "wynik" - czyli podmiana

# kurs Pythona: www.codecademy.com -> Learn -> Python
